﻿using Flags;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Raft;
using RaftCommon;
using ServiceStack.Text;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using ZeroMQ;

namespace RaftTests
{
    [TestClass]
    public class RaftTest
    {
        private StateMachine machine;
        private ZmqContext context;
        private InMemoryFileSystem fs;
        private ZmqSocket raftClient, anotherPuller, somethingPuller;

        [TestInitialize]
        public void SetUp()
        {
            F.lags.ParseFlags(new string[] {
                "--client_address=inproc://cl",
                "--state_file=state"
            });
            context = ZmqContext.Create();
            fs = new InMemoryFileSystem();
            fs.Files["state"] = new PersistedState()
            {
                CurrentTerm = 0,
                Id = "me",
                VotedFor = null,
                Peers = new Dictionary<string, Peer>()
                {
                    {"another", new Peer() {
                        Address = "inproc://another",
                        Id = "another"
                    }},
                    {"something_else", new Peer() {
                        Address = "inproc://something_else",
                        Id = "something_else"
                    }}
                },
                Log = new List<LogEntry>()
                {
                    new LogEntry()
                    {
                        Index = 0,
                        Term = 0,
                        Command = new NopCommand()
                    }
                }
            };
            machine = new StateMachine(context, fs);

            raftClient = context.CreateSocket(SocketType.PUSH);
            raftClient.Bind(StateMachine.kIncomingAddress);
            anotherPuller = context.CreateSocket(SocketType.PULL);
            anotherPuller.Bind(StateMachine.kOutgoingPrefix + "another");
            somethingPuller = context.CreateSocket(SocketType.PULL);
            somethingPuller.Bind(StateMachine.kOutgoingPrefix + "something_else");
            new Thread(machine.Start) { IsBackground = true }.Start();
        }

        [TestMethod]
        public void TestFollowerAppendEntries()
        {
            var ae = new Rpc() {Request = new AppendEntries()
            {
                CommitIndex = 1,
                LeaderId = "another",
                PrevLogIndex = 0,
                PrevLogTerm = 0,
                Term = 0,
                Entries = new List<LogEntry>()
                {
                    new LogEntry()
                    {
                        Index = 1,
                        Term = 0,
                        Command = new Deposit() { Amount = 20, WalletId = "1"}
                    }
                }
            }};
            raftClient.SendMessage("another", ae);

            var msg = anotherPuller.Receive(Encoding.UTF8);
            var resp = msg.FromJson<Response>();

            machine.stopReceiving = true;
            Assert.AreEqual(2, machine.state.Log.Count);
            Assert.AreEqual(1, machine.commitIndex);
            Assert.IsInstanceOfType(resp.RpcResponse, typeof(AppendEntriesResponse));
            var res = resp.RpcResponse as AppendEntriesResponse;
            Assert.AreEqual(1, res.LastLogIndex);
            Assert.AreEqual(0, res.Term);
            Assert.AreEqual(true, res.Success);
        }

        [TestMethod]
        public void TestFollowerRequestVote()
        {
            var rv = new Rpc()
            {
                Request = new RequestVote
                {
                    CandidateId = "another",
                    LastLogIndex = 0,
                    LastLogTerm = 0,
                    Term = 1
                }
            };
            raftClient.SendMessage("another", rv);

            var msg = anotherPuller.Receive(Encoding.UTF8);
            var resp = msg.FromJson<Response>();

            machine.stopReceiving = true;
            Assert.AreEqual(1, machine.state.CurrentTerm);
            Assert.AreEqual("another", machine.state.VotedFor);
            Assert.IsInstanceOfType(resp.RpcResponse, typeof(RequestVoteResponse));
            var res = resp.RpcResponse as RequestVoteResponse;
            Assert.AreEqual(1, res.Term);
            Assert.AreEqual(true, res.VoteGranted);
        }

        [TestMethod]
        public void TestCandidacy()
        {
            machine.mode = StateMachine.Mode.NewCandidate;
            var msg = anotherPuller.Receive(Encoding.UTF8);
            var anotherMsg = msg.FromJson<Rpc>();
            msg = somethingPuller.Receive(Encoding.UTF8);
            var somethingMsg = msg.FromJson<Rpc>();

            raftClient.SendMessage("another", new Response
            {
                RpcResponse = new RequestVoteResponse
                {
                    Term = 1,
                    VoteGranted = true
                }
            });
            raftClient.SendMessage("something_else", new Response
            {
                RpcResponse = new RequestVoteResponse
                {
                    Term = 1,
                    VoteGranted = true
                }
            });

            msg = anotherPuller.Receive(Encoding.UTF8);
            var heartbeat = msg.FromJson<Rpc>();
            machine.stopReceiving = true;

            Assert.IsInstanceOfType(anotherMsg.Request, typeof(RequestVote));
            Assert.IsInstanceOfType(somethingMsg.Request, typeof(RequestVote));
            var anotherRv = anotherMsg.Request as RequestVote;
            var somethingRv = somethingMsg.Request as RequestVote;
            Assert.AreEqual(1, anotherRv.Term);
            Assert.AreEqual(0, anotherRv.LastLogIndex);
            Assert.AreEqual(0, anotherRv.LastLogTerm);
            Assert.AreEqual("me", anotherRv.CandidateId);
            Assert.AreEqual(1, somethingRv.Term);
            Assert.AreEqual(0, somethingRv.LastLogIndex);
            Assert.AreEqual(0, somethingRv.LastLogTerm);
            Assert.AreEqual("me", somethingRv.CandidateId);
            Assert.IsInstanceOfType(heartbeat.Request, typeof(AppendEntries));
            var ae = heartbeat.Request as AppendEntries;
            Assert.AreEqual(0, ae.Entries.Count);
            Assert.AreEqual(-1, ae.CommitIndex);
            Assert.AreEqual("me", ae.LeaderId);
            Assert.AreEqual(0, ae.PrevLogIndex);
            Assert.AreEqual(0, ae.PrevLogTerm);
            Assert.AreEqual(1, ae.Term);
        }

        [TestMethod]
        public void TestLeadership()
        {
            machine.mode = StateMachine.Mode.Leader;
            new Thread(machine.ClientStart).Start();
            var msg = anotherPuller.Receive(Encoding.UTF8);
            var anotherHeartbeat = msg.FromJson<Rpc>();
            msg = somethingPuller.Receive(Encoding.UTF8);
            var somethingHeartbeat = msg.FromJson<Rpc>();

            var client = context.CreateSocket(SocketType.REQ);
            client.Connect("inproc://cl");
            client.Send(new ClientCommand
            {
                RequestId = "first",
                Command = new Deposit
                {
                    Amount = 1000,
                    WalletId = "wallet1"
                }
            }.ToJson<ClientCommand>(), Encoding.UTF8);
            AppendEntries ae = null;
            do
            {
                msg = anotherPuller.Receive(Encoding.UTF8);
                somethingPuller.Receive(Encoding.UTF8);
                ae = (AppendEntries)msg.FromJson<Rpc>().Request;
            } while (ae.Entries == null || ae.Entries.Count == 0);

            raftClient.SendMessage("another", new Response
            {
                RpcResponse = new AppendEntriesResponse
                {
                    LastLogIndex = 1,
                    Success = true,
                    Term = 0
                }
            });
            msg = client.Receive(Encoding.UTF8);
            var resp = msg.FromJson<ClientCommandResponse>();
            Assert.IsTrue(resp.Success);
            Assert.AreEqual("me", resp.LeaderId);
            Assert.IsTrue(machine.marketState.Wallets.ContainsKey("wallet1"));
            Assert.AreEqual(1000, machine.marketState.Wallets["wallet1"]);
        }
    }
}
