﻿using System;
using System.Collections.Generic;
using Raft;
using RaftCommon;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZeroMQ;

namespace RaftTests
{
    [TestClass]
    public class MarketTest
    {
        private StateMachine machine;

        [TestInitialize]
        public void SetUp()
        {
            machine = new StateMachine(ZmqContext.Create(), null);
            machine.state = new PersistedState();
            machine.state.Log = new List<LogEntry>();
            machine.commitIndex = -1;
        }

        [TestMethod]
        public void TestWallets()
        {
            machine.state.Log.Add(new LogEntry()
                {
                    Index = 0,
                    Term = 0,
                    Command = new Deposit() { Amount = 20, WalletId = "wallet" }
                });
            machine.commitUpTo(null, 0);
            Assert.AreEqual(20, machine.marketState.Wallets["wallet"]);
            machine.state.Log.Add(new LogEntry()
            {
                Index = 1,
                Term = 0,
                Command = new Withdraw() { Amount = 10, WalletId = "wallet" }
            });
            machine.commitUpTo(null, 1);
            Assert.AreEqual(10, machine.marketState.Wallets["wallet"]);
        }

        [TestMethod]
        public void TestPlaceOrder()
        {
            machine.marketState.Wallets["wallet1"] = 100;
            machine.marketState.Wallets["wallet2"] = 100;
            machine.state.Log.Add(new LogEntry()
            {
                Index = 0,
                Term = 0,
                Command = new PlaceOrder()
                {
                    Order = new Order()
                    {
                        IsBuy = true,
                        ItemId = "item",
                        OrderId = "order1",
                        Price = 20,
                        Quantity = 2,
                        WalletId = "wallet1"
                    }
                }
            });
            machine.commitUpTo(null, 0);
            Assert.AreEqual(1, machine.marketState.Orders.Count);
            Assert.AreEqual(1, machine.BuyOrders["item"].Count);
            Assert.AreEqual(0, machine.SellOrders["item"].Count);
            machine.state.Log.Add(new LogEntry()
            {
                Index = 0,
                Term = 0,
                Command = new PlaceOrder()
                {
                    Order = new Order()
                    {
                        IsBuy = false,
                        ItemId = "item",
                        OrderId = "order2",
                        Price = 10,
                        Quantity = 1,
                        WalletId = "wallet2"
                    }
                }
            });
            machine.commitUpTo(null, 1);
            Assert.AreEqual(1, machine.marketState.Orders.Count);
            Assert.AreEqual(1, machine.BuyOrders["item"].Count);
            Assert.AreEqual(0, machine.SellOrders["item"].Count);
            Assert.AreEqual(80, machine.marketState.Wallets["wallet1"]);
            Assert.AreEqual(120, machine.marketState.Wallets["wallet2"]);
            Assert.AreEqual(1, machine.marketState.Orders["order1"].Quantity);
        }
    }
}
