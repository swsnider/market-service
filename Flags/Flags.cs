﻿// Copyright 2013 Silas Snider
// All Rights Reserved.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace Flags
{
    public class DuplicateFlag : Exception
    {
        public DuplicateFlag()
        {
        }

        public DuplicateFlag(string message) : base(message)
        {
        }

        public DuplicateFlag(string message, Exception inner)
            : base(message, inner)
        {
        }
    }

    #region Flag Classes

    public abstract class FlagDef
    {
        internal bool Seen = false;
        internal string Name { get; set; }
        internal string Description { get; set; }
        internal abstract string defaultAsStr();
        internal abstract bool parseValue(string val);
        internal abstract void setValueToDefaultIfNeeded();
    }

    /// <summary>
    ///     Represents a boolean flag. See below for more details.
    /// </summary>
    public class BoolFlagDef : FlagDef
    {
        internal bool Default { get; set; }
        public bool Value { get; set; }

        internal override string defaultAsStr()
        {
            return Default ? "true" : "false";
        }

        internal override bool parseValue(string val)
        {
            if (val == "true" || val == "1" || val == "yes")
            {
                Value = true;
                Seen = true;
                return true;
            }
            if (val == "false" || val == "0" || val == "no")
            {
                Value = false;
                Seen = true;
                return true;
            }
            return false;
        }

        internal override void setValueToDefaultIfNeeded()
        {
            if (!Seen)
            {
                Value = Default;
            }
        }
    }

    /// <summary>
    ///     Represents a string flag. See below for more details.
    /// </summary>
    public class StringFlagDef : FlagDef
    {
        internal string Default { get; set; }
        public string Value { get; set; }

        internal override string defaultAsStr()
        {
            return "\"" + Default + "\"";
        }

        internal override bool parseValue(string val)
        {
            Value = val;
            Seen = true;
            return true;
        }

        internal override void setValueToDefaultIfNeeded()
        {
            if (!Seen)
            {
                Value = Default;
            }
        }
    }

    /// <summary>
    ///     Represents a string list flag. See below for more details.
    /// </summary>
    public class StringListFlagDef : FlagDef
    {
        internal StringListFlagDef()
        {
            Value = new List<string>();
        }

        internal List<string> Default { get; set; }
        public List<string> Value { get; set; }

        internal override string defaultAsStr()
        {
            return String.Join<string>(",", Default);
        }

        internal override bool parseValue(string val)
        {
            Value.AddRange(val.Split(",".ToCharArray()));
            Seen = true;
            return true;
        }

        internal override void setValueToDefaultIfNeeded()
        {
            if (!Seen)
            {
                Value.AddRange(Default);
            }
        }
    }

    /// <summary>
    ///     Represents an integer flag. See below for more details.
    /// </summary>
    public class IntFlagDef : FlagDef
    {
        internal int Default { get; set; }
        public int Value { get; set; }

        internal override string defaultAsStr()
        {
            return Default.ToString();
        }

        internal override bool parseValue(string val)
        {
            try
            {
                Value = int.Parse(val);
                Seen = true;
            }
            catch (FormatException)
            {
                return false;
            }
            catch (OverflowException)
            {
                return false;
            }
            return true;
        }

        internal override void setValueToDefaultIfNeeded()
        {
            if (!Seen)
            {
                Value = Default;
            }
        }
    }

    /// <summary>
    ///     Represents a float flag. See below for more details.
    /// </summary>
    public class FloatFlagDef : FlagDef
    {
        internal float Default { get; set; }
        public float Value { get; set; }

        internal override string defaultAsStr()
        {
            return Default.ToString();
        }

        internal override bool parseValue(string val)
        {
            try
            {
                Value = float.Parse(val);
                Seen = true;
            }
            catch (FormatException)
            {
                return false;
            }
            catch (OverflowException)
            {
                return false;
            }
            return true;
        }

        internal override void setValueToDefaultIfNeeded()
        {
            if (!Seen)
            {
                Value = Default;
            }
        }
    }

    /// <summary>
    ///     Represents an integer list flag. See below for more details.
    /// </summary>
    public class IntListFlagDef : FlagDef
    {
        internal IntListFlagDef()
        {
            Value = new List<int>();
        }

        internal List<int> Default { get; set; }
        public List<int> Value { get; set; }

        internal override string defaultAsStr()
        {
            return String.Join(",", Default);
        }

        internal override bool parseValue(string val)
        {
            try
            {
                string[] args = val.Split(",".ToCharArray());
                foreach (string arg in args)
                {
                    Value.Add(int.Parse(arg));
                }
                Seen = true;
            }
            catch (FormatException)
            {
                return false;
            }
            catch (OverflowException)
            {
                return false;
            }
            return true;
        }

        internal override void setValueToDefaultIfNeeded()
        {
            if (!Seen)
            {
                Value.AddRange(Default);
            }
        }
    }

    /// <summary>
    ///     Represents an enum flag. See below for more details.
    /// </summary>
    public class EnumFlagDef : FlagDef
    {
        internal string Default { get; set; }
        public string Value { get; set; }
        internal List<string> Choices { get; set; }

        internal override string defaultAsStr()
        {
            return Default;
        }

        internal override bool parseValue(string val)
        {
            if (Choices.IndexOf(val) == -1)
            {
                return false;
            }
            Value = val;
            Seen = true;
            return true;
        }

        internal override void setValueToDefaultIfNeeded()
        {
            if (!Seen)
            {
                Value = Default;
            }
        }
    }

    #endregion

    /// <summary>
    ///     Implements command-line flag parsing.
    /// </summary>
    public sealed class F
    {
        private static readonly F instance = new F();
        private static bool hasScannedForFlags;
        private readonly Dictionary<string, FlagDef> flags;
        private readonly Dictionary<string, bool> seenFiles;

        private F()
        {
            flags = new Dictionary<string, FlagDef>();
            seenFiles = new Dictionary<string, bool>();
            BoolFlag("help", false, "show this help.");
            StringFlag("flagfile", "",
                       "Insert flag definitions from the given file into the command line.");
        }

        /// <summary>
        ///     Make sure that all classes in the AppDomain have had their
        ///     static constructors called, so that any flags they define get
        ///     registered.
        /// </summary>
        public static F lags
        {
            get
            {
                if (!hasScannedForFlags)
                {
                    foreach (
                        Assembly ass in AppDomain.CurrentDomain.GetAssemblies())
                    {
                        foreach (Type type in ass.GetTypes())
                        {
                            RuntimeHelpers.RunClassConstructor(type.TypeHandle);
                        }
                    }
                    hasScannedForFlags = true;
                }
                return instance;
            }
        }

        /// <summary>
        ///     Do the actual work of parsing the command-line arguments and setting
        ///     the appropriate flag definitions.
        /// </summary>
        /// <param name="args">
        ///     An array of strings representing the program's
        ///     commandline arguments. Typically passed in straight from your
        ///     main().
        /// </param>
        /// <returns>
        ///     A list of any command-line arguments that don't match a
        ///     registered flag.
        /// </returns>
        public List<string> ParseFlags(string[] args)
        {
            var ret = new List<string>();
            for (int i = 0; i < args.Length; i++)
            {
                string f = args[i].Trim();
                if (f == "--")
                {
                    i++;
                    ret.AddRange(new ArraySegment<string>(args, i,
                                                          args.Length - i));
                    return ret;
                }
                if (!f.StartsWith("--"))
                {
                    ret.Add(f);
                    continue;
                }
                string n = f.Substring(2);
                string a = null;
                if (n.IndexOf('=') > -1)
                {
                    string[] t = n.Split("=".ToCharArray(), 2);
                    n = t[0];
                    a = t[1];
                }
                if (!flags.ContainsKey(n))
                {
                    if (n.StartsWith("no") && flags.ContainsKey(n.Substring(2)))
                    {
                        flags[n.Substring(2)].parseValue("false");
                        continue;
                    }
                    displayError(
                        String.Format(
                            "Flag {0} specified, but not defined anywhere.", f));
                }
                if (a != null)
                {
                    if (n == "flagfile")
                    {
                        ret.AddRange(ParseFlags(getFlagFileFlags(a)));
                        continue;
                    }
                    flags[n].parseValue(a);
                    continue;
                }
                if (((i + 1) >= args.Length) || args[i + 1].StartsWith("--"))
                    // Flag has no args
                {
                    if (flags[n].GetType() != typeof (BoolFlagDef))
                    {
                        displayError(
                            String.Format(
                                "No argument specified for flag {0}.", f));
                    }
                    flags[n].parseValue("true");
                    // Boolean flag, so presence is equivalent to truth-setting.
                    continue;
                }
                if (n == "flagfile")
                {
                    ret.AddRange(ParseFlags(getFlagFileFlags(args[i + 1])));
                    continue;
                }
                if (flags[n].parseValue(args[i + 1]))
                {
                    i += 1;
                }
            }
            if (flags["help"].Seen)
            {
                displayHelp();
                Environment.Exit(0);
            }
            foreach (var t in flags)
            {
                t.Value.setValueToDefaultIfNeeded();
            }
            return ret;
        }

        /// <summary>
        ///     Reads in a list of flags from the given file.
        /// </summary>
        /// <remarks>
        ///     Flagfiles are newline-delimited lists of flags, possibly containing
        ///     comments.
        ///     In order to remain safe in the presence of possible --flagfile
        ///     cycles, <c>getFlagFileFlags()</c> keeps track of the filenames it
        ///     has read.
        /// </remarks>
        /// <param name="filename">The path to the flagfile to read.</param>
        /// <returns>
        ///     An array of strings appropriate for use with
        ///     <c>ParseFlags()</c>
        /// </returns>
        private string[] getFlagFileFlags(string filename)
        {
            if (seenFiles.ContainsKey(filename))
            {
                return new string[] {};
            }
            seenFiles[filename] = true;
            var st = new StreamReader(filename);
            var args = new List<string>();
            while (!st.EndOfStream)
            {
                string line = st.ReadLine();
                if (line != null)
                {
                    string l = line.Trim();
                    if (l.StartsWith("#") || l.StartsWith("//")) continue;
                    args.AddRange(l.Split(null));
                }
            }
            return args.ToArray<string>();
        }

        private void displayError(string p)
        {
            Console.WriteLine(p);
            displayHelp();
            Environment.Exit(1);
        }

        private void displayHelp()
        {
            string name = AppDomain.CurrentDomain.FriendlyName;
            var h = new StringBuilder();
            h.AppendLine(String.Format("USAGE: {0} [flags]\r\n", name));
            h.AppendLine("flags:\r\n");
            string[] s = flags.Keys.ToArray();
            Array.Sort(s, StringComparer.InvariantCulture);
            foreach (string i in s)
            {
                FlagDef f = flags[i];
                string n = i;
                if (f.GetType() == typeof (BoolFlagDef))
                {
                    n = "[no]" + n;
                }
                h.AppendLine(String.Format(
                    "  --{0}: {1}\r\n    (default: {2})", n, f.Description,
                    f.defaultAsStr()));
            }
            Console.Write(h.ToString());
        }

        #region Flag Registration Methods

        /// <summary>
        ///     Registers a boolean flag.
        /// </summary>
        /// <remarks>
        ///     Boolean flags are slightly special, in that the flag parser accepts
        ///     --noFLAGNAME as meaning --FLAGNAME=false.
        /// </remarks>
        /// <param name="name">The name of the flag on the commandline.</param>
        /// <param name="def">The default value for the flag.</param>
        /// <param name="desc">
        ///     The help text to display next to this flag in
        ///     the output of --help.
        /// </param>
        public BoolFlagDef BoolFlag(string name, bool def, string desc)
        {
            if (flags.ContainsKey(name))
            {
                throw new DuplicateFlag("Duplicate flag detected: " + name);
            }
            var f = new BoolFlagDef
            {
                Name = name,
                Default = def,
                Description = desc
            };
            flags[name] = f;
            return f;
        }

        /// <summary>
        ///     Registers a string flag.
        /// </summary>
        /// <param name="name">The name of the flag on the commandline.</param>
        /// <param name="def">The default value for the flag.</param>
        /// <param name="desc">
        ///     The help text to display next to this flag in
        ///     the output of --help.
        /// </param>
        public StringFlagDef StringFlag(string name, string def, string desc)
        {
            if (flags.ContainsKey(name))
            {
                throw new DuplicateFlag("Duplicate flag detected: " + name);
            }
            var f = new StringFlagDef
            {
                Name = name,
                Default = def,
                Description = desc
            };
            flags[name] = f;
            return f;
        }

        /// <summary>
        ///     Registers a string list flag.
        /// </summary>
        /// <remarks>
        ///     This is simply a string flag that can hold multiple values. This can
        ///     result from both passing a comma-delimited list to the flag on the
        ///     command line, or repeating the flag multiple times, or both.
        /// </remarks>
        /// <param name="name">The name of the flag on the commandline.</param>
        /// <param name="def">The default value for the flag.</param>
        /// <param name="desc">
        ///     The help text to display next to this flag in
        ///     the output of --help.
        /// </param>
        public StringListFlagDef StringListFlag(string name, List<string> def,
                                                string desc)
        {
            if (flags.ContainsKey(name))
            {
                throw new DuplicateFlag("Duplicate flag detected: " + name);
            }
            var f = new StringListFlagDef
            {
                Name = name,
                Default = def,
                Description = desc
            };
            flags[name] = f;
            return f;
        }

        /// <summary>
        ///     Registers an integer flag.
        /// </summary>
        /// <param name="name">The name of the flag on the commandline.</param>
        /// <param name="def">The default value for the flag.</param>
        /// <param name="desc">
        ///     The help text to display next to this flag in
        ///     the output of --help.
        /// </param>
        public IntFlagDef IntFlag(string name, int def, string desc)
        {
            if (flags.ContainsKey(name))
            {
                throw new DuplicateFlag("Duplicate flag detected: " + name);
            }
            var f = new IntFlagDef
            {
                Name = name,
                Default = def,
                Description = desc
            };
            flags[name] = f;
            return f;
        }

        /// <summary>
        ///     Registers a float flag.
        /// </summary>
        /// <param name="name">The name of the flag on the commandline.</param>
        /// <param name="def">The default value for the flag.</param>
        /// <param name="desc">
        ///     The help text to display next to this flag in
        ///     the output of --help.
        /// </param>
        public FloatFlagDef FloatFlag(string name, float def, string desc)
        {
            if (flags.ContainsKey(name))
            {
                throw new DuplicateFlag("Duplicate flag detected: " + name);
            }
            var f = new FloatFlagDef
            {
                Name = name,
                Default = def,
                Description = desc
            };
            flags[name] = f;
            return f;
        }

        /// <summary>
        ///     Registers an integer list flag.
        /// </summary>
        /// <remarks>
        ///     This is simply an integer flag that can hold multiple values. This
        ///     can result from both passing a comma-delimited list to the flag on
        ///     the command line, or repeating the flag multiple times, or both.
        /// </remarks>
        /// <param name="name">The name of the flag on the commandline.</param>
        /// <param name="def">The default value for the flag.</param>
        /// <param name="desc">
        ///     The help text to display next to this flag in
        ///     the output of --help.
        /// </param>
        public IntListFlagDef IntListFlag(string name, List<int> def,
                                          string desc)
        {
            if (flags.ContainsKey(name))
            {
                throw new DuplicateFlag("Duplicate flag detected: " + name);
            }
            var f = new IntListFlagDef
            {
                Name = name,
                Default = def,
                Description = desc
            };
            flags[name] = f;
            return f;
        }

        /// <summary>
        ///     Registers an enum flag.
        /// </summary>
        /// <param name="name">The name of the flag on the commandline.</param>
        /// <param name="def">The default value for the flag.</param>
        /// <param name="desc">
        ///     The help text to display next to this flag in
        ///     the output of --help.
        /// </param>
        /// <param name="choices">
        ///     A list of the valid values the flag can take.
        /// </param>
        public EnumFlagDef EnumFlag(string name, string def, string desc,
                                    List<string> choices)
        {
            if (flags.ContainsKey(name))
            {
                throw new DuplicateFlag("Duplicate flag detected: " + name);
            }
            var f = new EnumFlagDef
            {
                Name = name,
                Default = def,
                Description = desc,
                Choices = choices
            };
            flags[name] = f;
            return f;
        }

        #endregion
    }
}
