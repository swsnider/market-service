﻿// Copyright 2013 Silas Snider
// All Rights Reserved.

using Flags;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace WebServer
{
    public class Http
    {
        public delegate void RequestHandler(HttpListenerContext ctx);

        private static readonly StringListFlagDef prefixes =
            F.lags.StringListFlag("prefix", new List<string> {"http://+:8080/"},
                                  "Prefixes to register with HttpListener.");

        private static Http instance;
        private Dictionary<string, RequestHandler> handlers;

        private Http()
        {
            handlers = new Dictionary<string, RequestHandler>();
        }

        public static Http Server
        {
            get
            {
                if (instance == null)
                {
                    instance = new Http();
                }
                return instance;
            }
        }

        public void RegisterHandler(string url, RequestHandler handler)
        {
            handlers[url] = handler;
        }

        /// <summary>
        /// Handles an individual web request, routing it appropriately.
        /// </summary>
        /// <param name="arg"></param>
        private void handleRequest(object arg)
        {
            var context = arg as HttpListenerContext;
            if (context == null)
            {
                return;
            }
            string url = context.Request.Url.AbsolutePath;
            if (handlers == null)
            {
                ServeString(context, "No handlers were registered at all -- " +
                                     "this is definitely a bug.", 500);
                return;
            }

            var sortedHandlers = from h in handlers.Keys
                                 orderby h descending
                                 select h;

            foreach (var pattern in sortedHandlers)
            {
                var handler = handlers[pattern];
                var n = pattern.Length;
                if (n == 0) continue;
                if (!pattern.EndsWith("/"))
                {
                    if (pattern == url)
                    {
                        handler(context);
                        return;
                    }
                }
                else if (url.Length >= n &&
                         (url.Substring(0, n - 1) == pattern ||
                          url.Substring(0, n) == pattern))
                {
                    handler(context);
                    return;
                }
            }
            //Nothing matched?
            ServeString(context, "Something...strange... happened.", 500);
        }

        /// <summary>
        /// Starts up the HTTP server.
        /// </summary>
        public void ListenAndServeForever()
        {
            var listener = new HttpListener();
            foreach (string s in prefixes.Value) listener.Prefixes.Add(s);
            try
            {
                listener.Start();
            }
            catch (HttpListenerException e)
            {
                Console.WriteLine("Got exception starting http listener: " +
                                  e.Message);
                Console.WriteLine("If this is an access denied exception, run" +
                                  " the following command as an admin user:");
                Console.WriteLine("netsh http add urlacl url=<PREFIX> " +
                                  "user=DOMAIN\\user");
                Console.WriteLine("for each prefix you specified on the " +
                                  "command line.");
                return;
            }
            while (true)
            {
                HttpListenerContext context = listener.GetContext();
                new Thread(handleRequest).Start(context);
            }
        }

        public static void ServeString(HttpListenerContext context, string s,
                                       int code = 200)
        {
            context.Response.StatusCode = code;
            byte[] buffer = Encoding.UTF8.GetBytes(s);
            context.Response.ContentLength64 = buffer.Length;
            context.Response.OutputStream.Write(buffer, 0, buffer.Length);
            context.Response.Close();
        }
    }
}
