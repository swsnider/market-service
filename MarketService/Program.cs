﻿// Copyright 2013 Silas Snider
// All Rights Reserved.

using System.Threading;
using Flags;
using Raft;
using WebServer;

namespace MarketService
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            F.lags.ParseFlags(args);
            new Thread(Http.Server.ListenAndServeForever)
            {
                IsBackground = true
            }.Start();
            new Server().ListenAndServeForever();
        }
    }
}
