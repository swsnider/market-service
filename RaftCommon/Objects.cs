﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace RaftCommon
{
    public interface IFileSystemProxy
    {
        T ReadObjectFromFile<T>(string path) where T : class;
        void WriteObjectToFile<T>(string path, T obj) where T : class;
    }

    public class InMemoryFileSystem : IFileSystemProxy
    {
        public Dictionary<string, object> Files { get; set; }

        public InMemoryFileSystem()
        {
            Files = new Dictionary<string, object>();
        }

        public T ReadObjectFromFile<T>(string path) where T : class
        {
            if (!Files.ContainsKey(path))
            {
                throw new IOException("No entry for path " + path);
            }
            var o = Files[path] as T;
            if (o == null)
            {
                throw new IOException("Unable to cast the object.");
            }
            return o;
        }

        public void WriteObjectToFile<T>(string path, T obj) where T : class
        {
            Files[path] = obj;
        }
    }

    public class PersistedState
    {
        public int CurrentTerm { get; set; }
        public string Id { get; set; }
        public List<LogEntry> Log { get; set; }
        public Dictionary<string, Peer> Peers { get; set; }
        public string VotedFor { get; set; }
    }

    public class Peer
    {
        public string Address { get; set; }
        public string Id { get; set; }
    }

    public interface IMessage
    {
    }

    public interface IRpc
    {
    }

    public class Rpc
    {
        public IRpc Request { get; set; }
    }

    public interface IResponse
    {
    }

    public class Response
    {
        public IResponse RpcResponse { get; set; }
    }

    public interface ICommand
    {
    }

    public interface IReadonlyCommand : ICommand
    {
        ICommandResponse handleCommand(MarketState state);
    }

    public interface ICommandResponse
    {
    }

    public class RequestVote : IRpc
    {
        public string CandidateId { get; set; }
        public int LastLogIndex { get; set; }
        public int LastLogTerm { get; set; }
        public int Term { get; set; }

        public bool LogIsCompleteAs(List<LogEntry> list)
        {
            int listLastTerm = 0;
            if (list.Count > 0) listLastTerm = list[list.Count - 1].Term;
            if (listLastTerm < LastLogTerm)
            {
                return true;
            }
            if (listLastTerm > LastLogTerm)
            {
                return false;
            }

            return (list.Count - 1) <= LastLogIndex;
        }
    }

    public class RequestVoteResponse : IResponse
    {
        public int Term { get; set; }
        public bool VoteGranted { get; set; }
    }

    public class AppendEntries : IRpc
    {
        public int CommitIndex { get; set; }
        public List<LogEntry> Entries { get; set; }
        public string LeaderId { get; set; }
        public int PrevLogIndex { get; set; }
        public int PrevLogTerm { get; set; }
        public int Term { get; set; }
    }

    public class AppendEntriesResponse : IResponse
    {
        public int LastLogIndex { get; set; }
        public bool Success { get; set; }
        public int Term { get; set; }
    }

    public class LogEntry
    {
        public ICommand Command { get; set; }
        public int Index { get; set; }
        public int Term { get; set; }
    }

    public class Deposit : ICommand
    {
        public int Amount { get; set; }
        public string WalletId { get; set; }
    }

    public class Withdraw : ICommand
    {
        public int Amount { get; set; }
        public string WalletId { get; set; }
    }

    public class GetBalance : IReadonlyCommand
    {
        public string WalletId { get; set; }

        public ICommandResponse handleCommand(MarketState state)
        {
            var resp = new GetBalanceResponse
            {
                Amount = (state.Wallets.ContainsKey(WalletId))
                    ? state.Wallets[WalletId]
                    : 0
            };
            return resp;
        }
    }

    public class GetBalanceResponse : ICommandResponse
    {
        public int Amount { get; set; }
    }

    public class Order
    {
        public bool IsBuy { get; set; }
        public string ItemId { get; set; }
        public string OrderId { get; set; }
        public int Price { get; set; }
        public int Quantity { get; set; }
        public string WalletId { get; set; }
    }

    public class PlaceOrder : ICommand
    {
        public Order Order { get; set; }
    }

    public class RetractOrder : ICommand
    {
        public string OrderId { get; set; }
    }

    public class ListOrders : IReadonlyCommand
    {
        // Only one of these should be set.
        public string ItemId { get; set; }
        public string WalletId { get; set; }

        public ICommandResponse handleCommand(MarketState state)
        {
            var resp = new ListOrdersResponse {
                Orders = new List<Order>(from o in state.Orders
                                         where o.Value.ItemId == ItemId ||
                                               o.Value.WalletId == WalletId
                                         select o.Value)
            };
            return resp;
        }
    }

    public class ListOrdersResponse : ICommandResponse
    {
        public List<Order> Orders { get; set; }
    }

    public class GetOrder : IReadonlyCommand
    {
        public string OrderId { get; set; }

        public ICommandResponse handleCommand(MarketState state)
        {
            var resp = new GetOrderResponse
            {
                Order = (state.Orders.ContainsKey(OrderId))
                    ? state.Orders[OrderId]
                    : null
            };
            return resp;
        }
    }

    public class GetOrderResponse : ICommandResponse
    {
        public Order Order { get; set; }
    }

    public class NopCommand : ICommand
    {
    }

    public class MarketState
    {
        public Dictionary<string, Order> Orders { get; set; }
        public Dictionary<string, int> Wallets { get; set; }

        public MarketState()
        {
            Wallets = new Dictionary<string, int>();
            Orders = new Dictionary<string, Order>();
        }
    }

    public class ClientCommand
    {
        public ICommand Command { get; set; }
        public string RequestId { get; set; }
    }

    public class ClientCommandResponse
    {
        public string LeaderId { get; set; }
        public ICommandResponse Response { get; set; }
        public bool Success { get; set; }
    }
}
