﻿// Copyright 2013 Silas Snider
// All Rights Reserved.

using System.Text;
using ServiceStack.Text;
using ZeroMQ;

namespace Raft
{
    /// <summary>
    /// Convenience extension function for <c>ZmqSocket</c>s.
    /// </summary>
    public static class ZmqSocketExtension
    {
        /// <summary>
        /// Sends an rpc message through a socket that understands 0MQ peer
        /// addressing.
        /// </summary>
        /// <param name="sock">The <c>ZmqSocket</c> to use.</param>
        /// <param name="id">The id of the peer to communicate with.</param>
        /// <param name="obj">The object to send.</param>
        /// <returns></returns>
        public static SendStatus SendMessage(this ZmqSocket sock, string id,
                                             object obj)
        {
            return sock.SendMessage(Encoding.UTF8.GetBytes(id), obj);
        }

        public static SendStatus SendMessage(this ZmqSocket sock, byte[] id,
                                             object obj)
        {
            var msg = new ZmqMessage();
            msg.Append(id);
            msg.AppendEmptyFrame();
            if (obj == null) msg.AppendEmptyFrame();
            else msg.Append(Encoding.UTF8.GetBytes(obj.ToJson<object>()));
            return sock.SendMessage(msg);
        }
    }
}
