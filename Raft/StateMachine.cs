﻿// Copyright 2013 Silas Snider
// All Rights Reserved.

using Flags;
using RaftCommon;
using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using WebServer;
using ZeroMQ;

namespace Raft
{
    /// <summary>
    ///     Provides an implementation of both the raft protocol and our market
    ///     service protocol built on top of raft.
    /// </summary>
    internal class StateMachine
    {
        internal static readonly string kTimerAddress = "inproc://timer";
        internal static readonly string kIncomingAddress = "inproc://incoming";
        internal static readonly string kOutgoingPrefix = "inproc://dealer";

        internal static readonly string kClientIncomingAddress =
            "inproc://clientpuller";

        internal static readonly string kTriggerAddress = "inproc://trigger";

        #region Flags

        internal static readonly IntFlagDef minTimeout = F.lags.IntFlag(
            "min_timeout", 150,
            "Minimum number of milliseconds before elections timeout.");

        internal static readonly StringFlagDef stateFilename = F.lags.StringFlag(
            "state_file", "", "Path to save state snapshots to.");

        internal static readonly StringFlagDef clientAddress = F.lags.StringFlag(
            "client_address", "tcp://*:5556",
            "ZMQ address to use for client RPCs.");

        #endregion

        internal readonly ZmqContext context;
        internal readonly IFileSystemProxy filesystem;
        internal ZmqSocket raftSocket, clientSocket;
        internal Random rng;

        internal PersistedState state;

        #region Timer State

        internal readonly AutoResetEvent watchdogTimer =
            new AutoResetEvent(false);

        internal ZmqSocket timeSocket;
        internal int timeout;
        internal Stopwatch watchdog;

        #endregion

        #region Ephemeral State

        internal Dictionary<string, ICommandResponse> cachedResponses;
        internal int commitIndex;
        internal string currentLeader;
        internal Dictionary<int, List<byte[]>> currentTriggers;
        internal Mode mode;
        internal Dictionary<string, int> nextIndices;
        internal List<int> numStored;
        internal bool stopReceiving;
        internal int votesForMe;
        internal Dictionary<string, List<Order>> BuyOrders;
        internal Dictionary<string, List<Order>> SellOrders;
        internal MarketState marketState;

        internal enum Mode
        {
            Follower,
            NewCandidate,
            Candidate,
            Leader
        }

        #endregion

        internal StateMachine(ZmqContext context, IFileSystemProxy fs)
        {
            this.context = context;
            filesystem = fs;
            stopReceiving = false;
            marketState = new MarketState();
            BuyOrders = new Dictionary<string, List<Order>>();
            SellOrders = new Dictionary<string, List<Order>>();
            currentTriggers = new Dictionary<int, List<byte[]>>();
            Http.Server.RegisterHandler("/debug/vars", HandleVarsRequest);
        }

        internal void updateTimeout()
        {
            timeout = rng.Next(minTimeout.Value, minTimeout.Value*2);
        }

        public void HandleVarsRequest(System.Net.HttpListenerContext ctx)
        {
            var b = new StringBuilder();
            b.AppendFormat("currentTerm: {0}\n", state.CurrentTerm);
            b.AppendFormat("numLogEntries: {0}\n", state.Log.Count);
            b.AppendFormat("votedFor: {0}\n", state.VotedFor);
            b.AppendFormat("commitIndex: {0}\n", commitIndex);
            b.AppendFormat("mode: {0}\n", Enum.GetName(typeof(Mode), mode));
            b.AppendFormat("numBuyOrders: {0}\n", BuyOrders.Count);
            b.AppendFormat("numSellOrders: {0}\n", SellOrders.Count);
            b.AppendFormat("numSellOrders: {0}\n", SellOrders.Count);
            Http.ServeString(ctx, b.ToString(), 200);
        }

        #region Persistent State Functions

        /// <summary>
        /// Attempts to restore the raft protocol state from a file. If it
        /// fails, it forcibly exits the program.
        /// </summary>
        /// <param name="filename">The path to the state file.</param>
        internal void RestoreState(string filename)
        {
            try
            {
                state = filesystem.ReadObjectFromFile<PersistedState>(filename);
            }
            catch (Exception)
            {
                Environment.Exit(1);
            }

            if (state == null)
            {
                Environment.Exit(1);
            }
        }

        /// <summary>
        /// Attempts to save the raft protocol state to a file. If it fails, it
        /// forcibly exits the program.
        /// </summary>
        /// <param name="filename">The path to the state file.</param>
        internal void SaveState(string filename)
        {
            try
            {
                filesystem.WriteObjectToFile<PersistedState>(filename, state);
            }
            catch (Exception)
            {
                Environment.Exit(1);
            }
        }

        #endregion

        #region Threads

        /// <summary>
        /// Keeps track of elapsed time so that we remember to send heartbeat
        /// packets (if leader), or start elections (if not).
        /// </summary>
        internal void StartTimer()
        {
            rng = new Random();
            watchdog = new Stopwatch();
            updateTimeout();
            watchdog.Start();

            ZmqSocket timeServer = context.CreateSocket(SocketType.PAIR);
            timeServer.Connect(kTimerAddress);
            var empty = new byte[] {};

            while (!watchdogTimer.WaitOne(minTimeout.Value/2))
            {
                // Trigger on election timeout, or every so often for Leader
                // heartbeat purposes.
                if (timeout <= watchdog.ElapsedMilliseconds ||
                    mode == Mode.Leader)
                {
                    timeServer.Send(empty);
                    watchdog.Restart();
                    updateTimeout();
                }
            }
        }

        /// <summary>
        /// Handles the main loop for the raft protocol state machine.
        /// </summary>
        internal void Start()
        {
            RestoreState(stateFilename.Value);
            mode = Mode.Follower;
            currentLeader = null;
            commitIndex = -1;

            timeSocket = context.CreateSocket(SocketType.PAIR);
            timeSocket.ReceiveReady += onTimer;
            timeSocket.Bind(kTimerAddress);

            raftSocket = context.CreateSocket(SocketType.PULL);
            raftSocket.ReceiveReady += onRaftRpc;
            raftSocket.Connect(kIncomingAddress);

            new Thread(StartTimer).Start();

            var poller = new Poller(new List<ZmqSocket>
            {
                timeSocket,
                raftSocket
            });
            while (!stopReceiving)
            {
                switch (mode)
                {
                    case Mode.Follower:
                        SaveState(stateFilename.Value);
                        poller.Poll();
                        break;
                    case Mode.NewCandidate:
                        startCandidacy();
                        break;
                    case Mode.Leader:
                        startLeader();
                        break;
                }
            }
        }

        /// <summary>
        /// Handles incoming RPCs from market service clients.
        /// </summary>
        internal void ClientStart()
        {
            cachedResponses = new Dictionary<string, ICommandResponse>();
            clientSocket = context.CreateSocket(SocketType.ROUTER);
            clientSocket.ReceiveReady += onClientRpc;
            clientSocket.Bind(clientAddress.Value);

            ZmqSocket puller = context.CreateSocket(SocketType.PULL);
            puller.ReceiveReady += onResponseToClient;
            puller.Bind(kClientIncomingAddress);

            var poller = new Poller(new List<ZmqSocket>
            {
                clientSocket,
                puller
            });

            while (!stopReceiving)
            {
                poller.Poll();
            }
        }

        /// <summary>
        /// Handles the lifecycle of a client RPC end-to-end.
        /// </summary>
        /// <param name="obj">The receieved <c>ZmqMessage</c></param>
        internal void StartClientMessageHandler(object obj)
        {
            var zmsg = obj as ZmqMessage;
            Debug.Assert(zmsg != null, "zmsg != null");
            byte[] zid = zmsg.First.Buffer;
            var msg = Encoding.UTF8.GetString(zmsg.Last.Buffer).
                               FromJson<ClientCommand>();
            ICommand cmd = msg.Command;
            Type cmdType = msg.Command.GetType();

            ZmqSocket sock = context.CreateSocket(SocketType.PUSH);
            sock.Connect(kClientIncomingAddress);
            ZmqSocket triggerSocket = context.CreateSocket(SocketType.REQ);
            triggerSocket.Connect(kTriggerAddress);

            if (cachedResponses.ContainsKey(msg.RequestId))
            {
                sock.SendMessage(zid, new ClientCommandResponse
                {
                    LeaderId = state.Id,
                    Response = cachedResponses[msg.RequestId],
                    Success = true
                });
                sock.Close();
                return;
            }

            if (cmdType == typeof (IReadonlyCommand))
            {
                sock.SendMessage(zid, new ClientCommandResponse
                {
                    LeaderId = state.Id,
                    Response = ((IReadonlyCommand) cmd).handleCommand(marketState),
                    Success = true
                });
                sock.Close();
                return;
            }

            if (cmdType == typeof (PlaceOrder))
            {
                ((PlaceOrder) cmd).Order.OrderId = Guid.NewGuid().ToString();
            }

            int idx;
            lock (state.Log)
            {
                idx = state.Log.Count;
                state.Log.Add(new LogEntry
                {
                    Command = cmd,
                    Index = state.Log.Count,
                    Term = state.CurrentTerm
                });
                numStored.Add(1);
            }
            triggerSocket.Send(Encoding.UTF8.GetBytes(idx.ToJson()));
            triggerSocket.ReceiveMessage();
            triggerSocket.Close();
            sock.SendMessage(zid, new ClientCommandResponse
            {
                LeaderId = state.Id,
                Response = null,
                Success = true
            });
            sock.Close();
        }

        #endregion

        #region Socket Event Handlers

        /// <summary>
        /// Actually sends the heartbeat or starts a new election when the
        /// election timer goes off.
        /// </summary>
        internal void onTimer(object sender, SocketEventArgs e)
        {
            e.Socket.ReceiveMessage();
            if (mode != Mode.Leader)
            {
                //mode = Mode.NewCandidate;
            }
            else
            {
                int ni = state.Log.Count;
                var emptyAe = new Rpc
                {
                    Request = new AppendEntries
                        {
                            LeaderId = state.Id,
                            CommitIndex = commitIndex,
                            Entries = null,
                            PrevLogIndex = ni - 1,
                            PrevLogTerm = state.Log[ni - 1].Term,
                            Term = state.CurrentTerm
                        }
                }.ToJson<Rpc>();
                foreach (string p in state.Peers.Keys)
                {
                    ZmqSocket s = context.CreateSocket(SocketType.PUSH);
                    s.Connect(kOutgoingPrefix + p);
                    s.Send(emptyAe, Encoding.UTF8);
                    s.Close();
                }
            }
        }

        /// <summary>
        /// Handles all of the raft protocol RPCs when they arrive.
        /// </summary>
        internal void onRaftRpc(object sender, SocketEventArgs e)
        {
            ZmqMessage msg = e.Socket.ReceiveMessage();
            Peer peer =
                state.Peers[Encoding.UTF8.GetString(msg.First.Buffer)];
            var msgStr = Encoding.UTF8.GetString(msg.Last.Buffer);
            object rpc;
            if (msgStr.Contains("RpcResponse"))
            {
                rpc = msgStr.FromJson<Response>().RpcResponse;
            }
            else
            {
                rpc = msgStr.FromJson<Rpc>().Request;
            }
            Type t = rpc.GetType();
            if (t == typeof(RequestVote))
                handleRequestVote(peer,
                                  (RequestVote)rpc);
            else if (t == typeof(AppendEntries))
                handleAppendEntries(
                    peer, (AppendEntries)rpc);
            if (t == typeof(RequestVoteResponse))
                handleRequestVoteResponse((RequestVoteResponse)rpc);
            else if (t == typeof(AppendEntriesResponse))
                handleAppendEntriesResponse(peer, (AppendEntriesResponse)rpc);
        }

        /// <summary>
        /// Handles clients changing the log by sending out AppendEntries
        /// requests.
        /// </summary>
        internal void onLogChange(object sender, SocketEventArgs e)
        {
            ZmqMessage msg = e.Socket.ReceiveMessage();
            if (!msg.IsEmpty)
            {
                var neededIdx = Encoding.UTF8.GetString(msg.Last.Buffer).
                                         FromJson<int>();
                if (neededIdx <= commitIndex)
                {
                    e.Socket.SendMessage(msg.First.Buffer, null);
                    return;
                }

                if (!currentTriggers.ContainsKey(neededIdx))
                    currentTriggers[neededIdx] = new List<byte[]>();
                currentTriggers[neededIdx].Add(msg.First);
            }

            int lli;
            lock (state.Log)
            {
                lli = state.Log.Count;
            }
            foreach (string p in state.Peers.Keys)
            {
                if (nextIndices[p] < lli)
                {
                    ZmqSocket s = context.CreateSocket(SocketType.PUSH);
                    s.Connect(kOutgoingPrefix + p);
                    var entries = state.Log.GetRange(nextIndices[p],
                        lli - nextIndices[p]);
                    s.Send(new Rpc
                    {
                        Request = new AppendEntries
                            {
                                LeaderId = state.Id,
                                Term = state.CurrentTerm,
                                CommitIndex = commitIndex,
                                PrevLogTerm = state.Log[nextIndices[p] - 1].Term,
                                PrevLogIndex = nextIndices[p] - 1,
                                Entries = new List<LogEntry>(entries)
                            }
                    }.ToJson<Rpc>(), Encoding.UTF8);
                    s.Close();
                }
            }
        }

        /// <summary>
        /// Actually handles the client request, by adding a log entry if it's
        /// a mutation request, or reading fomr the state machine if it's a
        /// readonly request.
        /// </summary>
        internal void onClientRpc(object sender, SocketEventArgs e)
        {
            ZmqMessage msg = e.Socket.ReceiveMessage();
            if (mode != Mode.Leader)
            {
                e.Socket.SendMessage(msg.First,
                    new ClientCommandResponse
                        {
                            LeaderId = currentLeader,
                            Response = null,
                            Success = false
                        });
                return;
            }
            var ct = new Thread(StartClientMessageHandler) {IsBackground = true};
            ct.Start(msg);
        }

        /// <summary>
        /// Passes along messages from the main thread to the client thread.
        /// Required because 0MQ sockets can't be accessed from other threads.
        /// </summary>
        internal void onResponseToClient(object sender, SocketEventArgs e)
        {
            ZmqMessage msg = e.Socket.ReceiveMessage();
            clientSocket.SendMessage(msg);
        }

        #endregion

        #region State Handlers

        /// <summary>
        /// Main loop when we're the leader.
        /// </summary>
        internal void startLeader()
        {
            nextIndices = new Dictionary<string, int>();
            numStored = new List<int>();
            for (int i = 0; i < state.Log.Count; i++)
            {
                numStored.Add(1);
            }

            int ni = state.Log.Count;
            var emptyAe = new Rpc
            {
                Request = new AppendEntries
                    {
                        LeaderId = state.Id,
                        CommitIndex = commitIndex,
                        Entries = new List<LogEntry>(),
                        PrevLogIndex = ni - 1,
                        PrevLogTerm = state.Log[ni - 1].Term,
                        Term = state.CurrentTerm
                    }
            }.ToJson<Rpc>();
            foreach (string p in state.Peers.Keys)
            {
                nextIndices[p] = ni;
                ZmqSocket s = context.CreateSocket(SocketType.PUSH);
                s.Connect(kOutgoingPrefix + p);
                s.Send(emptyAe, Encoding.UTF8);
                s.Close();
            }

            ZmqSocket trigger = context.CreateSocket(SocketType.ROUTER);
            trigger.ReceiveReady += onLogChange;
            trigger.Bind(kTriggerAddress);
            var poller = new Poller(new List<ZmqSocket>
            {
                trigger,
                timeSocket,
                raftSocket
            });

            while (mode == Mode.Leader)
            {
                SaveState(stateFilename.Value);
                poller.Poll();
                commitIfNeeded(trigger);
            }
        }

        /// <summary>
        /// Main loop when we're a candidate.
        /// </summary>
        internal void startCandidacy()
        {
            mode = Mode.Candidate;
            votesForMe = 1;
            state.CurrentTerm++;
            state.VotedFor = state.Id;
            watchdog.Restart();

            var rv = new Rpc
            {
                Request = new RequestVote
                    {
                        CandidateId = state.Id,
                        LastLogIndex = state.Log.Count - 1,
                        LastLogTerm = state.Log[state.Log.Count - 1].Term,
                        Term = state.CurrentTerm
                    }
            }.ToJson<Rpc>();
            foreach (string p in state.Peers.Keys)
            {
                ZmqSocket s = context.CreateSocket(SocketType.PUSH);
                s.Connect(kOutgoingPrefix + p);
                s.Send(rv, Encoding.UTF8);
            }

            var majority = (int) Math.Ceiling(((double) state.Peers.Count + 1)/2);
            var poller = new Poller(new List<ZmqSocket>
            {
                timeSocket,
                raftSocket
            });
            while (votesForMe < majority)
            {
                SaveState(stateFilename.Value);
                poller.Poll();
                if (mode != Mode.Candidate) break;
            }
            if (votesForMe >= majority)
            {
                mode = Mode.Leader;
            }
        }

        #endregion

        #region RPC Message Handlers

        /// <summary>
        /// Determines whether to vote for a candidate that requests our vote.
        /// </summary>
        /// <param name="p">The peer requesting the vote.</param>
        /// <param name="vote">The actual RequestVote RPC object.</param>
        internal void handleRequestVote(Peer p, RequestVote vote)
        {
            ZmqSocket sock = context.CreateSocket(SocketType.PUSH);
            sock.Connect(kOutgoingPrefix + p.Id);

            if (vote.Term < state.CurrentTerm)
            {
                sock.Send(new Response
                {
                    RpcResponse = new RequestVoteResponse
                        {
                            Term = state.CurrentTerm,
                            VoteGranted = false
                        }
                }.ToJson<Response>(), Encoding.UTF8);
                sock.Close();
                return;
            }

            if (vote.Term > state.CurrentTerm)
            {
                state.CurrentTerm = vote.Term;
                mode = Mode.Follower;
                state.VotedFor = null;
            }

            if (state.VotedFor == null || state.VotedFor == vote.CandidateId)
            {
                if (vote.LogIsCompleteAs(state.Log))
                {
                    watchdog.Restart();
                    state.VotedFor = vote.CandidateId;
                    sock.Send(new Response
                    {
                        RpcResponse = new RequestVoteResponse
                            {
                                Term = state.CurrentTerm,
                                VoteGranted = true
                            }
                    }.ToJson<Response>(), Encoding.UTF8);
                    sock.Close();
                    return;
                }
            }
            sock.Send(new Response
            {
                RpcResponse = new RequestVoteResponse
                    {
                        Term = state.CurrentTerm,
                        VoteGranted = false
                    }
            }.ToJson<Response>(), Encoding.UTF8);
            sock.Close();
        }

        /// <summary>
        /// Encapsulates the logic around replicating log entries from a leader.
        /// </summary>
        /// <param name="p">The peer object representing the leader.</param>
        /// <param name="rpc">The actual RPC object.</param>
        internal void handleAppendEntries(Peer p, AppendEntries rpc)
        {
            var lastIdx = state.Log.Count - 1;
            ZmqSocket sock = context.CreateSocket(SocketType.PUSH);
            sock.Connect(kOutgoingPrefix + p.Id);

            if (rpc.Term < state.CurrentTerm)
            {
                sock.Send(new Response
                {
                    RpcResponse = new AppendEntriesResponse
                        {
                            LastLogIndex = lastIdx,
                            Term = state.CurrentTerm,
                            Success = false
                        }
                }.ToJson<Response>(), Encoding.UTF8);
                sock.Close();
                return;
            }

            if (rpc.Term > state.CurrentTerm)
            {
                state.CurrentTerm = rpc.Term;
                mode = Mode.Follower;
                state.VotedFor = null;
            }

            watchdog.Restart();
            currentLeader = rpc.LeaderId;

            if (lastIdx < rpc.PrevLogIndex ||
                state.Log[rpc.PrevLogIndex].Term != rpc.PrevLogTerm)
            {
                sock.Send(new Response
                {
                    RpcResponse = new AppendEntriesResponse
                        {
                            LastLogIndex = lastIdx,
                            Term = state.CurrentTerm,
                            Success = false
                        }
                }.ToJson<Response>(), Encoding.UTF8);
                sock.Close();
                return;
            }

            if (rpc.Entries != null && rpc.Entries.Count > 0)
            {
                lock (state.Log)
                {
                    int firstIdx = rpc.Entries[0].Index;
                    if (firstIdx < state.Log.Count)
                    {
                        state.Log.RemoveRange(firstIdx,
                                              state.Log.Count - firstIdx);
                    }
                    lock (state.Log)
                    {
                        state.Log.AddRange(rpc.Entries);
                        lastIdx = state.Log.Count - 1;
                    }
                }
            }

            if (rpc.CommitIndex > commitIndex)
            {
                commitUpTo(null, Math.Min(rpc.CommitIndex, lastIdx));
            }

            sock.Send(new Response
            {
                RpcResponse = new AppendEntriesResponse
                    {
                        LastLogIndex = lastIdx,
                        Term = state.CurrentTerm,
                        Success = true
                    }
            }.ToJson<Response>(), Encoding.UTF8);
            sock.Close();
        }

        /// <summary>
        /// Does appropriate recordkeeping when clients respond to one of our
        /// AppendEntries requests. If needed, commits log entries.
        /// </summary>
        /// <param name="p">The follower which responded.</param>
        /// <param name="appendEntriesResponse">The RPC object.</param>
        internal void handleAppendEntriesResponse(
            Peer p, AppendEntriesResponse appendEntriesResponse)
        {
            if (appendEntriesResponse.Term > state.CurrentTerm)
            {
                state.CurrentTerm = appendEntriesResponse.Term;
                mode = Mode.Follower;
                state.VotedFor = null;
                return;
            }

            if (mode != Mode.Leader) return;

            if (!appendEntriesResponse.Success)
            {
                nextIndices[p.Id]--;
                var triggerSocket = context.CreateSocket(SocketType.PUSH);
                triggerSocket.Connect(kTriggerAddress);
                triggerSocket.SendMessage(new ZmqMessage());
                triggerSocket.Close();
            }
            else
            {
                for (var i = nextIndices[p.Id];
                     i <= appendEntriesResponse.LastLogIndex;
                     i++)
                {
                    numStored[i]++;
                }
                nextIndices[p.Id] = appendEntriesResponse.LastLogIndex + 1;
            }
        }

        /// <summary>
        /// Records a peer's vote either for or against us.
        /// </summary>
        /// <param name="requestVoteResponse">The RPC object.</param>
        internal void handleRequestVoteResponse(
            RequestVoteResponse requestVoteResponse)
        {
            if (requestVoteResponse.VoteGranted)
            {
                Interlocked.Increment(ref votesForMe);
            }
            else if (requestVoteResponse.Term > state.CurrentTerm)
            {
                state.CurrentTerm = requestVoteResponse.Term;
                mode = Mode.Follower;
                votesForMe = 0;
                state.VotedFor = null;
            }
        }

        #endregion

        #region Market State Machine

        /// <summary>
        /// Determines whether we should commit log entries.
        /// </summary>
        /// <param name="trigger">The trigger socket used by <c>commitUpTo</c>
        /// </param>
        internal void commitIfNeeded(ZmqSocket trigger)
        {
            var majority = (int) Math.Ceiling(((double) state.Peers.Count + 1)/2);
            int n = commitIndex;
            for (int i = commitIndex + 1; i < numStored.Count; i++)
            {
                if (numStored[i] >= majority) n = i;
            }
            if (n > commitIndex)
            {
                int cti = -1;
                foreach (LogEntry l in state.Log)
                {
                    if (l.Term == state.CurrentTerm)
                    {
                        cti = l.Index;
                        break;
                    }
                }
                if (cti > -1 && n >= cti)
                {
                    commitUpTo(trigger, n);
                }
            }
        }

        /// <summary>
        /// Commits all log entries up to a given log index.
        /// </summary>
        /// <param name="trigger">The trigger socket through which to notify
        /// waiting clients.</param>
        /// <param name="idx">The target commitIndex</param>
        internal void commitUpTo(ZmqSocket trigger, int idx)
        {
            for (int i = commitIndex + 1; i <= idx; i++)
            {
                commitState((dynamic) state.Log[i].Command);
                commitIndex++;
                if (trigger == null || !currentTriggers.ContainsKey(i))
                    continue;
                foreach (var zid in currentTriggers[i])
                {
                    trigger.SendMessage(zid, null);
                }
                currentTriggers.Remove(i);
            }
        }

        /// <summary>
        /// Writes a deposit amount to <c>marketState.Wallets</c>
        /// </summary>
        internal void commitState(Deposit deposit)
        {
            if (!marketState.Wallets.ContainsKey(deposit.WalletId))
            {
                marketState.Wallets[deposit.WalletId] = 0;
            }
            marketState.Wallets[deposit.WalletId] += deposit.Amount;
        }

        /// <summary>
        /// Removes the withdrawal amount from <c>marketState.Wallets</c>
        /// </summary>
        internal void commitState(Withdraw withdrawal)
        {
            if (!marketState.Wallets.ContainsKey(withdrawal.WalletId))
            {
                return;
            }
            marketState.Wallets[withdrawal.WalletId] -= withdrawal.Amount;
        }

        /// <summary>
        /// Adds a new order to the market by first trying to match it with
        /// existing orders, then, as a last resort, adding it to the list of
        /// orders.
        /// </summary>
        internal void commitState(PlaceOrder place)
        {
            Order nOrder = place.Order;
            if (nOrder.Quantity <= 0) return;
            if (!BuyOrders.ContainsKey(place.Order.ItemId))
            {
                BuyOrders[nOrder.ItemId] = new List<Order>();
            }
            if (!SellOrders.ContainsKey(nOrder.ItemId))
            {
                SellOrders[nOrder.ItemId] = new List<Order>();
            }
            if (!marketState.Wallets.ContainsKey(nOrder.WalletId))
            {
                marketState.Wallets[nOrder.WalletId] = 0;
            }

            marketState.Orders[nOrder.OrderId] = place.Order;
            if (nOrder.IsBuy)
            {
                var removed = new Dictionary<string, bool>();
                foreach (Order o in SellOrders[nOrder.ItemId])
                {
                    if (o.Price <= nOrder.Price)
                    {
                        int numSold = (o.Quantity >= nOrder.Quantity)
                            ? nOrder.Quantity
                            : o.Quantity;
                        if ((numSold*nOrder.Price) > marketState.Wallets[
                            nOrder.WalletId])
                        {
                            continue;
                        }
                        marketState.Wallets[nOrder.WalletId] -=
                            (numSold*nOrder.Price);
                        marketState.Wallets[o.WalletId] +=
                            (numSold*nOrder.Price);
                        o.Quantity -= numSold;
                        nOrder.Quantity -= numSold;
                        if (o.Quantity == 0)
                        {
                            marketState.Orders.Remove(o.OrderId);
                            removed[o.OrderId] = true;
                        }
                        if (nOrder.Quantity == 0)
                        {
                            marketState.Orders.Remove(nOrder.OrderId);
                            break;
                        }
                    }
                }
                SellOrders[nOrder.ItemId].RemoveAll(
                    item => removed.ContainsKey(item.OrderId));
                if (marketState.Orders.ContainsKey(nOrder.OrderId))
                    BuyOrders[nOrder.ItemId].Add(nOrder);
            }
            else
            {
                var removed = new Dictionary<string, bool>();
                foreach (Order o in BuyOrders[nOrder.ItemId])
                {
                    if (o.Price >= nOrder.Price)
                    {
                        int numSold = (o.Quantity >= nOrder.Quantity)
                            ? nOrder.Quantity
                            : o.Quantity;
                        if ((numSold*o.Price) > marketState.Wallets[
                            o.WalletId])
                        {
                            continue;
                        }
                        marketState.Wallets[o.WalletId] -= (numSold*o.Price);
                        marketState.Wallets[nOrder.WalletId] +=
                            (numSold*o.Price);
                        o.Quantity -= numSold;
                        nOrder.Quantity -= numSold;
                        if (o.Quantity == 0)
                        {
                            marketState.Orders.Remove(o.OrderId);
                            removed[o.OrderId] = true;
                        }
                        if (nOrder.Quantity == 0)
                        {
                            marketState.Orders.Remove(nOrder.OrderId);
                            break;
                        }
                    }
                }
                BuyOrders[nOrder.ItemId].RemoveAll(
                    item => removed.ContainsKey(item.OrderId));
                if (marketState.Orders.ContainsKey(nOrder.OrderId))
                    SellOrders[nOrder.ItemId].Add(nOrder);
            }
        }

        /// <summary>
        /// Remove an order from the list of orders in the market.
        /// </summary>
        internal void commitState(RetractOrder retraction)
        {
            if (!marketState.Orders.ContainsKey(retraction.OrderId)) return;
            Order ord = marketState.Orders[retraction.OrderId];
            Dictionary<string, List<Order>> ords = (ord.IsBuy)
                ? BuyOrders
                : SellOrders;
            ords[ord.ItemId].Remove(ord);
            marketState.Orders.Remove(retraction.OrderId);
        }

        /// <summary>
        /// Do nothing.
        /// </summary>
        internal void commitState(NopCommand cmd)
        {
        }

        #endregion
    }
}
