﻿// Copyright 2013 Silas Snider
// All Rights Reserved.

using Flags;
using RaftCommon;
using ServiceStack.Text;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using ZeroMQ;

namespace Raft
{
    /// <summary>
    /// Encapsulates the socket interface to the external world.
    /// </summary>
    public class Server
    {
        #region Flags

        private static readonly StringFlagDef raftAddress = F.lags.StringFlag(
            "address", "tcp://*:5555", "ZMQ address to use for raft RPCs.");

        #endregion

        private readonly StateMachine machine;

        #region 0MQ Objects

        private readonly Thread clientThread;
        private readonly ZmqContext context;
        private readonly Dictionary<string, Thread> peerThreads;
        private readonly Thread machineThread;

        #endregion

        public Server()
        {
            context = ZmqContext.Create();
            machine = new StateMachine(context, new LocalFileSystem());
            machineThread = new Thread(machine.Start);
            clientThread = new Thread(machine.ClientStart);
            peerThreads = new Dictionary<string, Thread>();
        }

        /// <summary>
        /// Starts up the raft/market state machine, and listens for incoming
        /// traffic for the rest of time.
        /// </summary>
        public void ListenAndServeForever()
        {
            var router = context.CreateSocket(SocketType.ROUTER);
            router.Bind(raftAddress.Value);
            var publisher = context.CreateSocket(SocketType.PUSH);
            publisher.Bind(StateMachine.kIncomingAddress);

            setupPeerConnections();
            machineThread.Start();
            clientThread.Start();

            while (!machine.stopReceiving && machineThread.IsAlive)
            {
                var msg = router.ReceiveMessage();
                publisher.SendMessage(msg);
            }
        }

        /// <summary>
        /// Starts a thread to handle each of the configured peers in the raft
        /// network.
        /// </summary>
        private void setupPeerConnections()
        {
            foreach (Peer p in machine.state.Peers.Values)
            {
                peerThreads[p.Id] = new Thread(peerTask) { IsBackground = true };
                peerThreads[p.Id].Start(p);
            }
        }

        /// <summary>
        /// Handles communication with an individual peer in the raft fabric.
        /// </summary>
        /// <param name="obj">The peer to interface with.</param>
        private void peerTask(object obj)
        {
            var p = obj as Peer;
            Debug.Assert(p != null, "Argument to dealerTask was not Peer");
            var peerConnection = context.CreateSocket(SocketType.PUSH);
            peerConnection.Identity = Encoding.ASCII.GetBytes(machine.state.Id);
            peerConnection.Connect(p.Address);

            var puller = context.CreateSocket(SocketType.PULL);
            puller.Bind(StateMachine.kOutgoingPrefix + p.Id);

            while (machineThread.IsAlive)
            {
                var msg = puller.ReceiveMessage();
                peerConnection.SendMessage(msg);
            }
        }
    }

    class LocalFileSystem : IFileSystemProxy
    {
        public T ReadObjectFromFile<T>(string path) where T : class
        {
            FileStream f = null;
            try
            {
                f = new FileStream(path, FileMode.Open, FileAccess.Read);
                return JsonSerializer.DeserializeFromStream<T>(f);
            }
            finally
            {
                if (f != null) f.Close();
            }
        }

        public void WriteObjectToFile<T>(string path, T obj) where T : class
        {
            FileStream f = null;
            try
            {
                f = new FileStream(path, FileMode.Create, FileAccess.Write);
                JsonSerializer.SerializeToStream<T>(obj, f);
            }
            finally
            {
                if (f != null) f.Close();
            }
        }
    }
}
