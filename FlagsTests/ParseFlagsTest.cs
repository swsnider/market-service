﻿// Copyright 2013 Silas Snider
// All Rights Reserved.

using System.Collections.Generic;
using Flags;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FlagsTests
{
    [TestClass]
    public class ParseFlagsTest
    {
        [TestMethod]
        public void TestAllTheFlags()
        {
            StringFlagDef stringTestFlag = F.lags.StringFlag("string_test", "",
                                                             "");
            StringListFlagDef stringListTestFlag =
                F.lags.StringListFlag("string_list_test", null, "");
            IntFlagDef intTestFlag = F.lags.IntFlag("int_test", 0, "");
            BoolFlagDef boolTestFlag = F.lags.BoolFlag("bool_test", true, "");
            FloatFlagDef floatTestFlag = F.lags.FloatFlag("float_test", 0, "");
            IntListFlagDef intListTestFlag = F.lags.IntListFlag(
                "int_list_test", null, "");
            EnumFlagDef enumTestFlag = F.lags.EnumFlag("enum_test", null, "",
                                                       new List<string>
                                                       {
                                                           "choice1",
                                                           "choice2"
                                                       });

            var args = new[]
            {
                "--string_test=testing", "--string_list_test=some,args",
                "--string_list_test=another", "--int_test=20", "--nobool_test",
                "--float_test=79.6", "--int_list_test=89,91",
                "--int_list_test=92", "--enum_test", "choice1"
            };
            List<string> leftovers = F.lags.ParseFlags(args);
            Assert.AreEqual(0, leftovers.Count);
            Assert.AreEqual("testing", stringTestFlag.Value);
            CollectionAssert.AreEqual(
                new List<string> {"some", "args", "another"},
                stringListTestFlag.Value);
            Assert.AreEqual(20, intTestFlag.Value);
            Assert.IsFalse(boolTestFlag.Value);
            Assert.AreEqual((float) 79.6, floatTestFlag.Value);
            CollectionAssert.AreEqual(new List<int> {89, 91, 92},
                                      intListTestFlag.Value);
            Assert.AreEqual("choice1", enumTestFlag.Value);
        }
    }
}
